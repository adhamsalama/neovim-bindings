# Neovim Bindings

Bindings for plugins I use.

## Editing

### Copy
`y`

### Paste
`p`

### Cut selection
`d`

### Cut character
`x`

## File explorer

### Open/Close
`leader + e` 

### Create file
`a` inside the directory you want

### Rename file
`e`

### Remove file
`d`

### Switch back to/from editor
`C + l` to jump to tree

`C + h` to jump to editor

## Search
### Files
`leader + f`
### Content
`leader + s + t`

## Lazygit


### Open/Close
`leader + gg`

### Fetch
`f`

### Pull
`p`

### Push
`shift + p`

### Commit
`c`

### Stage specific file/directory
`space`

### Stage all files
`a`

### Discard all changes (staged and unstaged)
`d`


# Resources

- [The Primagen](https://www.youtube.com/playlist?list=PLm323Lc7iSW_wuxqmKx_xxNtJC_hJbQ7R)
- [LunarVim](https://www.lunarvim.org/docs/beginners-guide/keybinds-overview)
- [Vim Cheat Sheet](https://vim.rtorr.com/)
- [Nvim tree](https://github.com/nvim-tree/nvim-tree.lua)
- [LazyGit](https://github.com/jesseduffield/lazygit/blob/master/docs/keybindings/Keybindings_en.md) 
